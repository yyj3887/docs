import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLogin: false,
    DaoPost: [
      {
        id: null,
        title: null,
        text: null,
        img: null,
        createTime: null,
        user: null
      }
    ]
  },
  getters: {}
});
