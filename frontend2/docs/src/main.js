// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import Header from "./components/Header";
import AdminHeader from "./components/AdminHeder";
import Home from "./components/Home";
import Login from "./components/content/Login";
import router from "./router";
import Buefy from "buefy";
import "buefy/dist/buefy.css";
import axios from "axios"; // import axios
import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import store from "@/store";

Vue.use(BootstrapVue);
Vue.use(Buefy);
Vue.use(axios);

Vue.config.productionTip = false;
Vue.prototype.$axios = axios;

Vue.component("component-home", Home);
Vue.component("component-login", Login);
Vue.component("component-adminheader", AdminHeader);
Vue.component("component-header", Header);

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  components: { App },
  template: "<App/>"
});
