import Vue from "vue";
import App from "../App";
import Router from "vue-router";
import Home from "@/components/Home";
import Login from "@/components/content/Login";
import Post from "@/components/content/Post";
import NewEdit from "@/components/content/NewEdit";
import ListPage from "@/components/content/ListPage";
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
      // children: [
      //   {
      //     path: "/post/:id",
      //     name: "post",
      //     component: Post,
      //     props: r => ({
      //       id: Number(r.params.id)
      //     })
      //   }
      // ]
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/NewEdit",
      name: "newedit",
      component: NewEdit
    },
    {
      path: "/post/:id",
      name: "post",
      component: Post,
      props: r => ({
        id: Number(r.params.id)
      })
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  }
});
