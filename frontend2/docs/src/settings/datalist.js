const sampleDB = [
  {
    id: 0,
    title: "저는 김방울입니다.",
    text:
      "어느 날, 자취방에 놀러 온 친구가 고양이 한 마리를 쓰다듬으며 말했습니다.야. 너네집 고양이 예쁘다.",
    createTime: "2020.05.31",
    user: "yyj"
  },
  {
    id: 1,
    title: "저는 김루나입니다.",
    text: "나도 지금 처음 보는데...",
    createTime: "2020.05.31",
    user: "bk"
  },
  {
    id: 2,
    title: "저는 김방울2입니다.",
    text:
      "ㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋ 저만 웃겼나요.",
    createTime: "2020.05.31",
    user: "gm"
  },
  {
    id: 3,
    title: "저는 김방울3입니다.",
    text: "내 고양이 아닌데 시리즈를 소개합니다.",
    createTime: "2020.05.31",
    user: "admin"
  },
  {
    id: 4,
    title: "저는 김방울4입니다.",
    text:
      "새벽 5시반에 물 마시러 부엌에 왔는데 싱크대에 들어가 있는 녀석ㅋㅋㅋㅋㅋ",
    createTime: "2020.05.31",
    user: "yn"
  }
];

export default {
  find(id) {
    return sampleDB.find(obj => {
      return obj.id === id;
    });
  },
  getSampleDB() {
    return sampleDB;
  }
};
