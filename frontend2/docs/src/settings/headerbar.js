export default {
    create: {
        label: "글 쓰기"
    },
    read: {
        label: "글 읽기"
    },
    update: {
        label: "글 수정"
    },
    delete: {
        label: "글 삭제"
    }
}
