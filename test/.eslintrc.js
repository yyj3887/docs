module.exports = {
    extends: "eslint:recommended",
    rules: {
        "no-unexpected-multiline": "error",
        "no-undef": "off"
    }
}